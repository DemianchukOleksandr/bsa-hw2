﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using Newtonsoft.Json;
using CoolParking.BL.Models;
using System.ComponentModel;
using Newtonsoft.Json.Linq;

namespace CoolParking.UserInterface
{
    class CoolParkingService
    {
        private HttpClient _client;

        public CoolParkingService()
        {
            _client = new HttpClient();
        }

        public decimal GetBalance()
        {
            var result = _client.GetAsync(Settings.host + "/api/parking/balance").Result;
            if (result.IsSuccessStatusCode)
                return JsonConvert.DeserializeObject<decimal>(result.Content.ReadAsStringAsync().Result);
            else throw new Exception(result.Content.ReadAsStringAsync().Result);
        }

        public int GetCapacity()
        {
            var result = _client.GetAsync(Settings.host + "/api/parking/capacity").Result;
            if (result.IsSuccessStatusCode)
                return JsonConvert.DeserializeObject<int>(result.Content.ReadAsStringAsync().Result);
            else throw new Exception(result.Content.ReadAsStringAsync().Result);
        }

        public int GetFreePlaces()
        {
            var result = _client.GetAsync(Settings.host + "/api/parking/freePlaces").Result;
            if (result.IsSuccessStatusCode)
                return JsonConvert.DeserializeObject<int>(result.Content.ReadAsStringAsync().Result);
            else throw new Exception(result.Content.ReadAsStringAsync().Result);
        }

        public List<Vehicle> GetAllVehicles()
        {
            var result = _client.GetAsync(Settings.host + "/api/vehicles").Result;
            if (result.IsSuccessStatusCode)
                return JsonConvert.DeserializeObject<List<Vehicle>>(result.Content.ReadAsStringAsync().Result);
            else throw new Exception(result.Content.ReadAsStringAsync().Result);
        }

        public Vehicle GetVehicle(string id)
        {
            var result = _client.GetAsync(Settings.host + "/api/vehicles/" + id).Result;
            if (result.IsSuccessStatusCode)
                return JsonConvert.DeserializeObject<Vehicle>(result.Content.ReadAsStringAsync().Result);
            else throw new Exception(result.Content.ReadAsStringAsync().Result);
        }

        public string AddVehicle(Vehicle vehicle)
        {
            var content = new StringContent(JObject.FromObject(vehicle).ToString(), Encoding.UTF8, "application/json");
            var response = _client.PostAsync(Settings.host + "/api/vehicles", content).Result;

            if (response.IsSuccessStatusCode)
            {
                return "Vehicle added successfully.";
            }
            else throw new Exception(response.Content.ReadAsStringAsync().Result);
        }

        public string RemoveVehicle(string id)
        {
            var response = _client.DeleteAsync(Settings.host + "/api/vehicles/" + id).Result;

            if (response.IsSuccessStatusCode)
            {
                return "Vehicle deleted successfully.";
            }
            else throw new Exception(response.Content.ReadAsStringAsync().Result);
        }

        public TransactionInfo GetLastTransaction()
        {
            var result = _client.GetAsync(Settings.host + "/api/transactions/last").Result;
            if (result.IsSuccessStatusCode)
                return JsonConvert.DeserializeObject<TransactionInfo>(result.Content.ReadAsStringAsync().Result);
            else throw new Exception(result.Content.ReadAsStringAsync().Result);
        }

        public List<TransactionInfo> GetAllTransactions()
        {
            var result = _client.GetAsync(Settings.host + "/api/transactions/all").Result;
            if (result.IsSuccessStatusCode)
                return JsonConvert.DeserializeObject<List<TransactionInfo>>(result.Content.ReadAsStringAsync().Result);
            else throw new Exception(result.Content.ReadAsStringAsync().Result);
        }

        public string TopUpVehicle(string id, decimal Sum)
        {
            /*
            // Tried also this, but still doesn`t work.
            var values = new Dictionary<string, string>
            {
                { "id", id },
                { "Sum", Sum.ToString() }
            };

            var content = new FormUrlEncodedContent(values);

            */
            var content = new StringContent(JObject.FromObject(new
            {
                id = id,
                Sum = Sum.ToString()
            }).ToString(), Encoding.UTF8, "application/json");
            var response = _client.PutAsync(Settings.host + "/api/transactions/topUpVehicle", content).Result;

            if (response.IsSuccessStatusCode)
            {
                return "Vehicle topUped successfully.";
            }
            else throw new Exception(response.Content.ReadAsStringAsync().Result);
        }
    }
}
