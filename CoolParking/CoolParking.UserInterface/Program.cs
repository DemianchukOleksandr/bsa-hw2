﻿using System;
using System.Linq;
using CoolParking.BL;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;


namespace CoolParking.UserInterface
{
    class Program
    {
        static void Main(string[] args)
        {
            CoolParkingService coolParking = new CoolParkingService();

            Console.WriteLine("Hello lets` get started! (Use numbers to choose)" + Environment.NewLine);
            bool exit = false;
            string id = string.Empty;
            decimal balance = 0;
            while (!exit)
            {
                Console.WriteLine("What You want to do?" + Environment.NewLine +
                    "1) Current balance" + Environment.NewLine +
                    "2) Current capacity" + Environment.NewLine +
                    "3) Current free spaces" + Environment.NewLine +
                    "4) Vehicle by id" + Environment.NewLine +
                    "5) All Vehicles" + Environment.NewLine +
                    "6) Add Vehicle" + Environment.NewLine +
                    "7) Remove Vehicle" + Environment.NewLine +
                    "8) Last transaction" + Environment.NewLine +
                    "9) All transactions" + Environment.NewLine +
                    "10) TopUp Vehicle Balance" + Environment.NewLine +
                    "11) CLear" + Environment.NewLine +
                    "12) Exit" + Environment.NewLine);

                try
                {
                    switch (Console.ReadLine())
                    {
                        case "1":
                            Console.WriteLine("Current capacity is : " + coolParking.GetBalance() + Environment.NewLine);
                            break;
                        case "2":
                            Console.WriteLine("Current capacity is : " + coolParking.GetCapacity() + Environment.NewLine);
                            break;
                        case "3":
                            Console.WriteLine("Free spaces : " + coolParking.GetFreePlaces() + Environment.NewLine);
                            break;
                        case "4":
                            Console.Write("Enter Id : ");
                            id = Console.ReadLine();
                            Console.WriteLine("Vehicle : " + coolParking.GetVehicle(id) + Environment.NewLine);
                            break;
                        case "5":
                            Console.WriteLine();
                            Console.WriteLine("All vehicles : ");
                            foreach(var vehicle in coolParking.GetAllVehicles())
                            {
                                Console.WriteLine(vehicle);
                            }
                            Console.WriteLine();
                            break;
                        case "6":
                                Console.Write("Enter Id : ");
                                id = Console.ReadLine();
                                Console.Write("Avaible types :" + Environment.NewLine + "   1) PassengerCar" + Environment.NewLine +
                                "   2) Truck" + Environment.NewLine +
                                "   3) Bus" + Environment.NewLine +
                                "   4) Motorcycle" + Environment.NewLine);
                                Console.Write("Enter Type : ");
                                VehicleType type = VehicleType.PassengerCar;
                                var breakAdding = false;
                                switch (Console.ReadLine())
                                {
                                    case "1": type = VehicleType.PassengerCar; break;
                                    case "2": type = VehicleType.Truck; break;
                                    case "3": type = VehicleType.Bus; break;
                                    case "4": type = VehicleType.Motorcycle; break;
                                    default: Console.Write("Wrong Type number try again." + Environment.NewLine + Environment.NewLine); breakAdding = true; break;
                                }
                                if (breakAdding) break;
    
                                Console.Write("Enter balance : ");
                                balance = Convert.ToDecimal(Console.ReadLine());
                                Console.WriteLine(Environment.NewLine +

                                coolParking.AddVehicle(new Vehicle(id, type, balance))
                                +Environment.NewLine);
                                
                                /*
                                Console.WriteLine(Environment.NewLine +

                                coolParking.AddVehicle(new Vehicle("AA-1234-FF", VehicleType.Motorcycle, 3))
                                + Environment.NewLine);

                                Console.WriteLine(Environment.NewLine +

                                coolParking.AddVehicle(new Vehicle("AB-1234-FF", VehicleType.Motorcycle, 3))
                                + Environment.NewLine);

                                Console.WriteLine(Environment.NewLine +

                                coolParking.AddVehicle(new Vehicle("AC-1234-FF", VehicleType.Motorcycle, 3))
                                + Environment.NewLine);
                                */
                            break;
                        case "7":
                            Console.Write("Enter Id : ");
                            id = Console.ReadLine();
                            Console.WriteLine("Result : " + coolParking.RemoveVehicle(id) + Environment.NewLine);
                            break;
                        case "8":
                            Console.WriteLine("Result : " + coolParking.GetLastTransaction() + Environment.NewLine);
                            break;
                        case "9":
                            Console.WriteLine();
                            Console.WriteLine("All transactions : ");
                            foreach (var transaction in coolParking.GetAllTransactions())
                            {
                                Console.WriteLine(transaction);
                            }
                            Console.WriteLine();
                            break;
                        case "10":
                            Console.Write("Enter Id : ");
                            id = Console.ReadLine();
                            Console.Write("Enter sum : ");
                            balance = Convert.ToDecimal(Console.ReadLine());
                            Console.WriteLine(Environment.NewLine +
                            coolParking.TopUpVehicle(id, balance)
                            + Environment.NewLine);
                            break;
                        case "11":
                            Console.Clear();
                            break;
                        case "12":
                            exit = true;
                            break;
                        default:
                            Console.WriteLine("I don`t know this command, try again please." + Environment.NewLine);
                            break;
                    }
                }
                catch(Exception e)
                {
                    Console.WriteLine("Something gone wrong, please try again." + Environment.NewLine + "Error : " + e.Message + Environment.NewLine);
                }
            }
        }
    }
}
