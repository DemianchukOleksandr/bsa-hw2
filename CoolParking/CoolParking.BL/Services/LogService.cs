﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.
using System;
using System.IO;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath { get; private set; }

        private FileStream LogFileStream { get; set; }
        private StreamWriter LogStreamWriter { get; set; }
        private StreamReader LogStreamReader { get; set; }
        public LogService(string logPath)
        {
            LogPath = logPath;
            //LogFileStream = File.Create(logPath);
            //StreamWriter LogStreamWriter = new StreamWriter(LogFileStream);
            //StreamReader LogStreamReader = new StreamReader(LogFileStream);
        }

        public string Read()
        {
            if (!File.Exists(LogPath)) throw new InvalidOperationException("Log directory doesn`t exist.");
            return File.ReadAllText(LogPath);
        }

        public void Write(string logInfo)
        {
            File.AppendAllText(LogPath, logInfo + Environment.NewLine);
        }
        /*

        public string Read()
        {
            if (!File.Exists(LogPath)) throw new InvalidOperationException("Log directory doesn`t exist.");
            return LogStreamReader.ReadToEnd();
        }

        public void Write(string logInfo)
        {
            LogStreamWriter.Write(logInfo);
        }

        public void Dispose()
        {
            LogStreamReader.Dispose();
            LogStreamWriter.Dispose();
            LogFileStream.Dispose();
        }
*/
    }
}
