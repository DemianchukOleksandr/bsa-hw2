﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.

using System;
using System.IO;
using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService, IDisposable
    {
        public double Interval { get ; set; }

        Timer _timer;
        public TimerService(double interval)
        {
            Interval = interval;
            _timer = new Timer(interval);
            //_timer.Elapsed += new ElapsedEventHandler(Elapsed);
            _timer.AutoReset = true;
        }

        public event ElapsedEventHandler Elapsed;

        public void Dispose()
        {
            _timer.Dispose();
        }

        public void Start()
        {
            _timer.Enabled = true;
            _timer.Start();
        }

        public void Stop()
        {
            _timer.Enabled = false;
            _timer.Stop();
        }


    }
}
