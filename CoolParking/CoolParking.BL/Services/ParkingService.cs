﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using System;
using System.Collections.ObjectModel;
using System.Threading;
using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private ITimerService withdrawTimer;
        private ITimerService logTimer;
        private ILogService logService;

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            this.withdrawTimer = withdrawTimer;
            withdrawTimer.Elapsed += new ElapsedEventHandler(WithdrawEvent);
            this.logTimer = logTimer;
            logTimer.Elapsed += SaveLogEvent;
            this.logService = logService;

        }

        public void Start()
        {
            withdrawTimer.Start();
            logTimer.Start();
        }

        public void Stop()
        {
            withdrawTimer.Stop();
            logTimer.Stop();
        }

        public void AddVehicle(Vehicle vehicle)
        {

            Parking.GetInstance().AddVehicle(vehicle);
        }

        public void Dispose()
        {
            logTimer.Dispose();
        }

        public decimal GetBalance()
        {
            return Parking.GetInstance().Balance;
        }

        public int GetCapacity()
        {
            return Parking.GetInstance().Capacity;
        }

        public int GetFreePlaces()
        {
            return Parking.GetInstance().Capacity - Parking.GetInstance().Vehicles.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return Parking.GetInstance().Transactions.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(Parking.GetInstance().Vehicles);
        }

        public string ReadFromLog()
        {
            return logService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            Parking.GetInstance().RemoveVehicle(vehicleId);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            Parking.GetInstance().TopUpVehicle(vehicleId, sum);
        }

        private static void WithdrawEvent(Object source, ElapsedEventArgs e)
        {
            Parking.GetInstance().TakeMoneyForParking();
        }

        private void SaveLogEvent(Object source, ElapsedEventArgs e)
        {
            Parking.GetInstance().SaveTransactions(logService);  
        }
    }
}
