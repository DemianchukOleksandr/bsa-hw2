﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using System.Collections.Generic;
using System.Reflection;
using System.IO;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static string host = "https://localhost:44340";

        public static decimal balance = 0;
        public static int capacity = 10;
        public static int withdrawPeriod = 2;
        public static int logPeriod = 60;
        public static Dictionary<VehicleType, decimal> charges = new Dictionary<VehicleType, decimal>()
        { 
            { VehicleType.PassengerCar, 2 }, 
            { VehicleType.Truck, 5 }, 
            { VehicleType.Bus, 3.5M },  
            { VehicleType.Motorcycle, 1 }
        };
        public static decimal fineCoef = 2.5M;
        public static string LogFilePath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";
    }
}
