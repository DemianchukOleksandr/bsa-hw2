﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.WebSockets;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        static Parking parkingInstance;

        public decimal Balance { get; private set; }
        public int Capacity { get; private set; }
        public int PayPeriod { get; private set; }
        public int LogPeriod { get; private set; }
        public Dictionary<VehicleType, decimal> Charges { get; private set; }
        public decimal FineCoef { get; private set; }
        public List<Vehicle> Vehicles { get; private set; }
        public List<TransactionInfo> Transactions { get; private set; }

        Parking()
        {
            this.Balance = Settings.balance;
            this.Capacity = Settings.capacity;
            this.PayPeriod = Settings.withdrawPeriod;
            this.PayPeriod = Settings.logPeriod;
            this.Charges = Settings.charges;
            this.FineCoef = Settings.fineCoef;
            Vehicles = new List<Vehicle>();
            Transactions = new List<TransactionInfo>();
        }
        public static Parking GetInstance()
        {
            if (parkingInstance == null) parkingInstance = new Parking();
            return parkingInstance;
        }
        public void AddVehicle(Vehicle vehicle)
        {
            if (Vehicles.Count >= Capacity) throw new InvalidOperationException("Parking is full.");
            if(Vehicles.Any(x => x.Id == vehicle.Id)) throw new ArgumentException("Id already exists in parking.");
            Vehicles.Add(vehicle);
        }

        public void RemoveVehicle(string vehicleId)
        {
            if (Vehicles.Exists(x => x.Id == vehicleId)) Vehicles.RemoveAll(x => x.Id == vehicleId);
            else throw new ArgumentException("The vehicle was not found in parking.");
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum < 0) throw new ArgumentException("sum cannot be negative.");
            var _vahicle = Vehicles.FirstOrDefault(x => x.Id == vehicleId);

            if (_vahicle != null)
            {
                _vahicle.Balance += sum;
            }
            else throw new ArgumentException("Vehicle doesn`t exist.");
        }

        public void TakeMoneyForParking()
        {
            var transactionDate = DateTime.Now;
            decimal sum;
            foreach (var vehicle in Vehicles)
            {
                var vehiclePosBal = Math.Max(0, vehicle.Balance);
                if (vehiclePosBal < Charges[vehicle.VehicleType])
                {
                    sum = vehiclePosBal + (Charges[vehicle.VehicleType] - vehiclePosBal) * FineCoef;
                    Balance += sum;
                    vehicle.Balance -= sum;
                    Transactions.Add(new TransactionInfo() { Sum = sum, VehicleId = vehicle.Id, TransactionDate = transactionDate });
                }
                else
                {
                    sum = Charges[vehicle.VehicleType];
                    Balance += sum;
                    vehicle.Balance -= sum;
                    Transactions.Add(new TransactionInfo() { Sum = sum, VehicleId = vehicle.Id, TransactionDate = transactionDate });
                }

            }
        }

        public void SaveTransactions(ILogService logger)
        {
            var log = string.Empty;
            foreach (var transaction in Transactions)
            {
                log += transaction.ToString() + Environment.NewLine;
            }
            logger.Write(log);
            Transactions = new List<TransactionInfo>();
        }
    }
}