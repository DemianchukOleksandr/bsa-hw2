﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;
using System.Text.RegularExpressions;
using Fare;
using Newtonsoft.Json;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        [JsonProperty("id")]
        public string Id {get; set; }
        [JsonProperty("vehicleType")]
        public VehicleType VehicleType {get; set;}
        [JsonProperty("balance")]
        public decimal Balance {get; set; }

        static readonly string _idPattern = @"^([A-Z]{2})-([0-9]{4})-([A-Z]{2})$";

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            if(!(new Regex(_idPattern).IsMatch(id)) || balance < 0)
            {
                throw new ArgumentException();
            }

            this.Id = id;
            this.VehicleType = vehicleType;
            this.Balance = balance;
        }

        public Vehicle()
        { }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            Xeger xeger = new Xeger(_idPattern, new Random());
            return xeger.Generate();
        }

        public static bool IsValidId(string id)
        {
            return new Regex(_idPattern).IsMatch(id);
        }

        public override string ToString()
        {
            return "Id : " + Id + ", VehicleType " + VehicleType.ToString() + ", Balance " + Balance;
        }
    }
}