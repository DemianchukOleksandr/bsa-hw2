﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Models;
using static CoolParking.WebAPI.Common.Common;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        [HttpGet]
        public JsonResult GetVehicles()
        {
            return new JsonResult(parkingService.GetVehicles());
        }

        [HttpGet("{id}")]
        public JsonResult GetVehicleById(string id)
        {
            JsonResult responce;
            if (!Vehicle.IsValidId(id))
            {
                responce = new JsonResult("id is not valid.");
                responce.StatusCode = 400;
                return responce;
            }

            var vehicle = parkingService.GetVehicles().Where(x => x.Id == id).FirstOrDefault();
            if (vehicle == null)
            {
                responce = new JsonResult("id was not found.");
                responce.StatusCode = 404;
                return responce;
            }

            responce = new JsonResult(vehicle);
            responce.StatusCode = 200;
            return responce;
        }

        [HttpPost]
        public JsonResult AddVehicle([FromBody] Vehicle vehicle)
        {
            JsonResult responce;

            try
            {
                if (!Vehicle.IsValidId(vehicle.Id) || vehicle.Balance < 0)
                {
                    responce = new JsonResult("Invalid body input.");
                    responce.StatusCode = 400;
                    return responce;
                }
                
                parkingService.AddVehicle(vehicle);
                
                responce = new JsonResult("Success.");
                responce.StatusCode = 201;
                return responce;
            }
            catch (Exception e)
            {
                responce = new JsonResult("Something went wrong. Error : " + e);
                responce.StatusCode = 400;
                return responce;
            }
        }

        [HttpDelete("{id}")]
        public JsonResult DeleteVehicleById(string id)
        {
            JsonResult responce;
            if (!Vehicle.IsValidId(id))
            {
                responce = new JsonResult("id is not valid.");
                responce.StatusCode = 400;
                return responce;
            }

            if (!parkingService.GetVehicles().Any(x => x.Id == id))
            {
                responce = new JsonResult("id was not found.");
                responce.StatusCode = 404;
                return responce;
            }

            parkingService.RemoveVehicle(id);
            responce = new JsonResult("Deleted.");
            responce.StatusCode = 204;
            return responce;
        }
    }
}
