﻿using Microsoft.AspNetCore.Mvc;
using static CoolParking.WebAPI.Common.Common;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        [HttpGet("balance")]
        public decimal Balance()
        {
            return parkingService.GetBalance();
        }

        [HttpGet("capacity")]
        public int Capacity()
        {
            return parkingService.GetCapacity();
        }

        [HttpGet("freePlaces")]
        public JsonResult FreePlaces()
        {
            return new JsonResult(parkingService.GetFreePlaces()); 
        }
    }
}
