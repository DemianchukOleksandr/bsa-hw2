﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using static CoolParking.WebAPI.Common.Common;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        [HttpGet("last")]
        public JsonResult GetLastTransaction()
        {
            var transactions = parkingService.GetLastParkingTransactions();
            
            if (transactions.Length > 0)
                return new JsonResult(transactions.First(y => y.TransactionDate == transactions.Max(x => x.TransactionDate)));
            else
            {
                var Badresult = new JsonResult("Transaction list is empty.");
                Badresult.StatusCode = 404;
                return Badresult;
            }
                
        }

        [HttpGet("all")]
        public JsonResult GetAllTransactions()
        {
            JsonResult jsonResult;
            try
            {
                return new JsonResult(parkingService.GetLastParkingTransactions());
            } 
            catch (InvalidOperationException e)
            {
                jsonResult = new JsonResult(e.Message);
                jsonResult.StatusCode = 404;
                return jsonResult;
            }
            catch (Exception e)
            {
                jsonResult = new JsonResult(e.Message);
                jsonResult.StatusCode = 400;
                return jsonResult;
            }
        }

        [HttpPut("topUpVehicle")]
        public JsonResult TopUpVehicle(string id, decimal Sum)
        {
            JsonResult jsonResult;
            if (!parkingService.GetVehicles().Any(x => x.Id == id))
            {
                jsonResult = new JsonResult("Vehicle was not found.");
                jsonResult.StatusCode = 404;
                return jsonResult;
            }

            parkingService.TopUpVehicle(id, Sum);
            return new JsonResult(parkingService.GetVehicles().First(x => x.Id == id));
        }
    }
}
