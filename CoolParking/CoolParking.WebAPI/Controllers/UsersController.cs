﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CoolParking.WebAPI.Services;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private UsersService usersService;

        public UsersController()
        {
            usersService = new UsersService();
        }

        // GET api/values
        [HttpGet]
        public async Task<ActionResult<List<User>>> Get()
        {
            return Ok(await usersService.GetUsers());
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<ActionResult<User>> Get(int id)
        {
            return Ok(await usersService.GetUser(id));
        }
       
    }
}

